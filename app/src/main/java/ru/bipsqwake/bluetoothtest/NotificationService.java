package ru.bipsqwake.bluetoothtest;

import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.widget.Toast;

import ru.bipsqwake.bluetoothtest.BtArduino.Arduino;

public class NotificationService extends NotificationListenerService {
    Arduino arduino;
    SharedPreferences pref;
    public static final int NOTIFICATION_ID = 777;
    private static final String LOG_TAG = "NotificationService";

    public NotificationService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(LOG_TAG, "Created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        super.onStartCommand(intent, flags, startID);
        pref = getSharedPreferences("main", MODE_PRIVATE);
        if (!pref.getBoolean("init", false) || pref.getString("name", "").equals("")) {
            stopSelf();
        }
        arduino = new Arduino(pref.getString("name", ""), this);
        if (!arduino.init()) {
            stopSelf();
        }
        Notification notification = new Notification.Builder(this).setContentTitle("LALALA")
                .build();
        startForeground(NOTIFICATION_ID, notification);
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        updateWatch();
    }
    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        updateWatch();
    }

    private void updateWatch() {
        StatusBarNotification[] notifications = getActiveNotifications();
        arduino.update(notifications);
    }

    @Override
    public void onListenerConnected() {
        Log.i(LOG_TAG, "Listener connected");
    }

    @Override
    public void onDestroy() {
        arduino.disconnect();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

    }

    public void stop() {
        arduino.disconnect();
        stopSelf();
    }
}
