package ru.bipsqwake.bluetoothtest.BtArduino;

import android.app.Notification;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.service.notification.StatusBarNotification;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by 04.09.2016 on 04.09.2016.
 */

public class Arduino {
    String name = "defaultname";
    static String mUUID = "00001101-0000-1000-8000-00805F9B34FB";
    BluetoothDevice device = null;
    BluetoothSocket socket = null;
    OutputStream outputStream = null;
    InputStream inputStream = null;
    Queue<String> messageQueue = new LinkedBlockingQueue<>();
    Thread writingTread = null;
    Thread readingThread = null;
    Context context;
    List<Notification> notifs = new LinkedList<>();

    public Arduino (String name, Context context) {
        this.name = name;
        this.context = context;
    }

    public boolean init() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairs = adapter.getBondedDevices();
        for (BluetoothDevice a : pairs) {
            if (a.getName().equals(name)) {
                device = a;
            }
        }
        if (device == null) {
            //report
            return false;
        }
        try {
            socket = device.createRfcommSocketToServiceRecord(UUID.fromString(mUUID));
            socket.connect();
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
        } catch (IOException e) {
            return false;
        }
        writingTread = new Thread(new Writer());
        writingTread.start();
        readingThread = new Thread(new Reader());
        readingThread.start();

        return true;
    }

    public void disconnect() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String contents) {
        for (int i = 0; i < contents.length(); i += 20) {
            messageQueue.add(contents.substring(i, (i + 20 > contents.length()) ? (contents.length()) : (i + 20)));
        }
    }

    public void update(StatusBarNotification[] notifications) {
        StringBuilder builder = new StringBuilder();
        notifs.clear();
        for (StatusBarNotification a : notifications) {
            Notification notification = a.getNotification();
            if ((notification.flags & Notification.FLAG_FOREGROUND_SERVICE) > 0) {continue;}
            notifs.add(notification);
            String tmp = notification.extras.getString(Notification.EXTRA_TITLE);
            if (tmp != null) {
                builder.append(tmp.substring(0, Math.min(tmp.length(), 28)).replaceAll("[\\\\]", "\\\\").replaceAll("[+]", "\\\\+").replaceAll("[~]",  "\\\\~").replaceAll("[\\*]", "\\\\*"));
            } else builder.append("EmptyTitle");
            builder.append("~");
            tmp = notification.extras.getString(Notification.EXTRA_TEXT);
            if (tmp != null) {
                builder.append(tmp.substring(0, Math.min(tmp.length(), 28)).replaceAll("[\\\\]", "\\\\").replaceAll("[+]", "\\\\+").replaceAll("[~]",  "\\\\~").replaceAll("[\\*]", "\\\\*"));
            } else builder.append("EmptyText");
            builder.append("~");
            if (notification.actions != null && notification.actions.length > 0) {
                for (Notification.Action b: notification.actions) {
                    tmp = b.title.toString().substring(0, Math.min(b.title.toString().length(), 13));
                    builder.append(tmp.replaceAll("[\\\\]", "\\\\").replaceAll("[+]", "\\\\+").replaceAll("[~]",  "\\\\~").replaceAll("[\\*]", "\\\\*"));
                    builder.append("~");
                }
            }
            builder.append("*");
        }
        builder.append("+");
        Log.d("String to go", builder.toString());
        write(builder.toString());
    }

    private class Writer implements Runnable{
        @Override
        public void run() {
            while (true) {
                if (messageQueue.size() > 0) {
                    try {
                        Charset windows1251 = Charset.forName("windows-1251");
                        String tmp = messageQueue.poll();
                        outputStream.write(tmp.getBytes(windows1251));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class Reader implements Runnable {
        byte t[] = new byte[2];
        @Override
        public void run() {
            while (true) {
                try {
                    if (inputStream.available() >= 2) {
                        inputStream.read(t, 0, 2);
                        notifs.get(t[0]).actions[t[1]].actionIntent.send();
                    }
                } catch (IOException | PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
