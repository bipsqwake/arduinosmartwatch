package ru.bipsqwake.bluetoothtest;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {
    SharedPreferences pref;
    BluetoothAdapter bluetooth;
    ListView pairDevices;
    private static final int REQUEST_ENABLE_BT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bluetooth = BluetoothAdapter.getDefaultAdapter();
        if (bluetooth == null) {
            Toast.makeText(this, "Your device does not support bluetooth", Toast.LENGTH_SHORT).show();
        }
        pref = getSharedPreferences("main", MODE_PRIVATE);
        if (bluetooth.isEnabled()) {
            if (!pref.getBoolean("init", false)) {
                startInit();
            } else {
                start();
            }
        } else {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    void startInit() {
        if (!bluetooth.isEnabled()) {
            //report
            return;
        }
        Set<BluetoothDevice> pairs = bluetooth.getBondedDevices();
        if (pairs.size() <= 0) {
            //report
            return;
        }
        List<String> names = new LinkedList<>();
        for (BluetoothDevice a : pairs) {
            names.add(a.getName());
        }
        pairDevices = (ListView)findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item, names);
        pairDevices.setAdapter(adapter);
        pairDevices.setOnItemClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK) {
                    if (!pref.getBoolean("init", false))
                        startInit();
                    else {
                        start();
                    }
                } else {
                    //report
                }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("name", ((TextView)view).getText().toString());
        editor.putBoolean("init", true);
        editor.apply();
        start();
    }

    public void start() {
        startService(new Intent(this, NotificationService.class));
        startActivity(new Intent(this, ControlActivity.class));
    }
}
