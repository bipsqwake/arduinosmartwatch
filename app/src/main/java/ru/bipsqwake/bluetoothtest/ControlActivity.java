package ru.bipsqwake.bluetoothtest;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ControlActivity extends AppCompatActivity implements View.OnClickListener {
    Intent intent;

    Button start;
    Button stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        intent = new Intent(this, NotificationService.class);

        start = (Button) findViewById(R.id.startServiceButton);
        stop = (Button) findViewById(R.id.stopServiceButton);
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        start.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startServiceButton:
                startService(intent);
                start.setEnabled(false);
                stop.setEnabled(true);
                break;
            case R.id.stopServiceButton:
                stopService(intent);
                start.setEnabled(true);
                stop.setEnabled(false);
                break;
        }
    }
}
