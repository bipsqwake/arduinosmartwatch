#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h> 
#include <messager.h>

Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 6, 7);

void setup() {
  Serial.begin(9600);
  display.begin();              // Инициализация дисплея
  display.setContrast(40);      // Устанавливаем контраст
  display.setTextColor(BLACK);  // Устанавливаем цвет текста
  display.setTextSize(1);       // Устанавливаем размер текста
  display.clearDisplay();       // Очищаем дисплей
  display.setTextSize(1);
  display.display();
  delay(1000); 
}


using namespace std;

String str = "";
String test = "test";
char incomingByte;
bool hasSlash = false;
int currentBytes = 0;
Messager mesgr;


void endBlock() {
  Serial.write(1);
  currentBytes = 0;
}

void commit() {
  mesgr.commit();
  endBlock();
}

void loop() {
  
  if(currentBytes == 20) {
    endBlock();
  }
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    currentBytes++;
    if (hasSlash) {
      str += incomingByte;
      hasSlash = false;
    } else {
    switch (incomingByte) {
      case '/':
        hasSlash = true;
        break;
      case '+':
        commit();
        break;
      case '~':
        mesgr.add(str);
        str = "";
        break;
      default:
        str += incomingByte;
        break;
    }
    }
  }
  if (mesgr.needUpdate()) {
    display.clearDisplay();
    //display.print(utf8rus(test));
    for (int i = 0; i < 5; i++) {
      display.println(mesgr.get(i).getTitlePrev());
    }
    display.display();
    mesgr.setUpdated();
  }
}

