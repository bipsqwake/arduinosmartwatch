#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <screen.h>



Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 6, 7);

void setup() {
  Serial.begin(9600);
  display.begin();              // Инициализация дисплея
  display.setContrast(40);      // Устанавливаем контраст
  display.setTextColor(BLACK);  // Устанавливаем цвет текста
  display.setTextSize(1);       // Устанавливаем размер текста
  display.clearDisplay();       // Очищаем дисплей
  display.setTextSize(1);
  display.display();

  pinMode(A0, INPUT);
  pinMode(13, OUTPUT);
  pinMode(8, INPUT);
  pinMode(9, INPUT);
  pinMode(10, INPUT);
  delay(1000); 
}


using namespace std;

Screen screen;
int count = 0;
int toRead = -1;
char incomingByte;
String str = "";
bool hasSlash = false;

void loop() {
  if (Serial.available() > 0) {
    if (toRead <= 0) {
      toRead = Serial.available();
    } else {
      while (toRead > 0) {
      incomingByte = Serial.read();
      toRead--;
      if (hasSlash) {
        str += incomingByte;
        hasSlash = false;
      } else {
      switch (incomingByte) {
        case '\\':
          hasSlash = true;
          break;
        case '~':
          screen.getMessager().add(str);
          str = "";
          break;
        case '*':
          screen.getMessager().nextMes();
          break;
        case '+':
          screen.getMessager().commit();
          break;
        default:
          str += incomingByte;
          break;
      }
      }
      }
    }
  } else if (toRead == 0) {
    toRead = -1;
  }
  screen.update();
}

