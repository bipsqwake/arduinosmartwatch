#include <screen.h>


Screen::Screen() : pot(A0), display(3, 4, 5, 6, 7){
    display.begin();              // ������������� �������
    display.setContrast(40);      // ������������� ��������
    display.setTextColor(BLACK);  // ������������� ���� ������
    display.setTextSize(1);       // ������������� ������ ������
    display.clearDisplay();       // ������� �������
    display.setTextSize(1);
    display.display();
    right.attach(8);
    middle.attach(9);
    left.attach(10);
    right.interval(5);
    middle.interval(5);
    left.interval(5);
    toWrite[0] = toWrite[1] = 255;
}

Messager&
Screen::getMessager() {
    return messager;
}

void
Screen::update() {
    display.clearDisplay();
    int potVal = pot.get();
    int sliderVal = potVal * 40 / 51;
    if (!messager.isCritical()) {
        int num = messager.getNum();
        int value = potVal * num / 51;
        if (right.update() && right.fell()) {
            messager.get(value).setNext();
        }
        if (middle.update() && middle.fell()) {
            if (messager.get(value).getActionsNum() > 0) {
                toWrite[0] = value;
                toWrite[1] = messager.get(value).getCurrentActionNum();
                write();
            }
        }
        messager.update(display, value);
    } else {
        display.print("Updating...");
    }
    display.drawLine(83, sliderVal, 83, sliderVal + 5, BLACK);
    display.display();
}

void
Screen::write() {
    Serial.write(toWrite, 2);
    toWrite[0] = toWrite[1] = 255;
}