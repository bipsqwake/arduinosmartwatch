#pragma once
#include <messager.h>
#include <potentiometr.h>
#include <Bounce2.h>
#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class Screen {
    public:
        Screen();
        Messager& getMessager();
        void update();
        void write();
    private:
        Messager messager;
        Adafruit_PCD8544 display;
        Potentiometr pot;
        Bounce right;
        Bounce middle;
        Bounce left;
        int screenPos = 0;
        char toWrite[2];
};