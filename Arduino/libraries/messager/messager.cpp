#include "messager.h"

using namespace std;

void
Message::setTitle(String _title) {
    title = _title;
}

void
Message::setText(String _text) {
    text = _text;
}

String
Message::getTitle() {
    return title;
}

String
Message::getText() {
    return text;
}

int
Message::getActionsNum() {
    return actionsNum;
}

int
Message::getCurrentActionNum() {
    return currentAction;
}

void
Message::addAction(String _text) {
    if (counter < 5) {
        actions[counter++] = _text;
    }
}

void Message::commit() {
    actionsNum = counter;
    counter = 0;
}

String
Message::getAction(int num) {
    return actions[num];
}

String
Message::getCurrentAction() {
    return actions[currentAction];
}

void
Message::setNext() {
    if (++currentAction >= actionsNum) {
        currentAction = 0;
    }
}

int
Messager::getNum() {
    return num;
}

void
Messager::add(String text) {
    critical = true;
    switch (counter) {
        case 0:
            messages[mesToAdd].setTitle(text);
            break;
        case 1:
            messages[mesToAdd].setText(text);
            break;
        default:
            messages[mesToAdd].addAction(text);
            break;
    }
    counter++;
}

void
Messager::commit() {
    critical = false;
    num = mesToAdd;
    mesToAdd = 0;
    counter = 0;
}

void
Messager::nextMes() {
    counter = 0;
    messages[mesToAdd].commit();
    mesToAdd++;
}

Message&
Messager::get(int number) {
    return messages[number];
}

bool
Messager::isCritical() {
    return critical;
}

void 
Messager::update(Adafruit_PCD8544 display, int value) {
    display.print(messages[value].getTitle());
    display.drawLine(0, 16, 83, 16, BLACK);
    display.setCursor(0, 18);
    display.print(messages[value].getText());
    if (messages[value].getActionsNum() > 0) {
        display.fillRect(0, 36, 84, 12, BLACK);
        display.setCursor(2, 38);
        display.setTextColor(WHITE);
        display.print(messages[value].getCurrentAction());
        display.setTextColor(BLACK);
    }
}
