#pragma once
#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
using namespace std;

class Message {
    public:
        void setTitle(String _title);
        void setText(String _text);
        String getTitle();
        String getText();
        int getActionsNum();
        int getCurrentActionNum();
        void addAction(String text);
        String getAction(int num);
        String getCurrentAction();
        void commit();
        void setNext();
    private:
        String title;
        String text;
        String actions[5];
        int actionsNum = 0;
        int currentAction = 0;
        int counter = 0;
};

class Messager {
    public:
        int getNum();
        void add(String text);
        void nextMes();
        void commit();
        bool isCritical();
        Message& get(int number);
        void update(Adafruit_PCD8544 display, int value);
    private:
        Message messages[5];
        int num = 0;
        int counter = 0;
        int mesToAdd = 0;
        bool critical = false;
};

