#pragma once
#include <Arduino.h>

class Potentiometr {
    public:
        Potentiometr(int _pin);
        int get();
    private:
        int pin;
        int current;
};