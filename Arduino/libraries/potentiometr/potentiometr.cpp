#include "potentiometr.h"

Potentiometr::Potentiometr(int _pin) : pin(_pin) {
    current = analogRead(pin) / 20;
}

int
Potentiometr::get() {
    int newCurrent = analogRead(A0);
    bool change = false;
    while (!change)
    if (current - 20 > newCurrent) {
        current = current - 20;
    } else if (current + 20 < newCurrent) {
        current = current + 20;
    } else {change = true;};
    return current / 20;
}